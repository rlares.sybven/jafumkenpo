<!DOCTYPE HTML>
<!--
    Theory by TEMPLATED
    templated.co @templatedco
    Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html>
    <head>
        <title>Jafum Kenpo Studio</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="{{ url('../resources/css/main.css') }}" />
        <link rel="icon" type="image/png" href="{{url('../resources/images/jfumkenpologo.png')}}">
    </head>
    <body class="subpage">

        <!-- Header -->
            <header id="header">
                <div class="inner">
                    <a href="index.html" class="logo"><img src="{{url('../resources/images/jfumkenpologo.png')}}" alt="logo jafumkenpo" /></a>
                    <nav id="nav">
                        <a href="{{ url('./') }}">Inicio</a>
                        <a href="{{ url('/contact') }}">Contactanos</a>
                        <!-- <a href="elements.html">Elementos</a> -->
                    </nav>
                    <a href="#navPanel" class="navPanelToggle"><span class="fa fa-bars"></span></a>
                </div>
            </header>

        

        <!-- Main -->
            <section id="main" class="wrapper">
                <div class="inner">
                    <header class="align-center">
                        <h2>Contacto</h2>
                        <p>Sede principal Parque los Caobos Caracas</p>
                    </header>

                <!-- Content -->
                    <div class="row">
                        <div class="6u 12u$(small)">
                            <a href="https://goo.gl/maps/DmGDp9DFbkipHX3v5" class="mapa"><img src="{{url('../resources/images/mapaloscaobos.jpg')}}" alt="mapa los caobos jafumkenpo" /></a>
                        </div>
                        <div class="6u$ 12u$(small)">
                            <h3>Para Informacion adicional</h3>

                                <form method="post" action="#">
                                    <div class="row uniform">
                                        <div class="6u 12u$(xsmall)">
                                            <input type="text" name="name" id="name" value="" placeholder="Nombre" />
                                        </div>
                                        <div class="6u$ 12u$(xsmall)">
                                            <input type="email" name="email" id="email" value="" placeholder="Correo" />
                                        </div>
                                        <div class="12u$">
                                            <textarea name="message" id="message" placeholder="Ingrese su consulta" rows="6"></textarea>
                                        </div>
                                        <!-- Break -->
                                        <div class="12u$">
                                            <ul class="actions">
                                                <li><input type="submit" value="Enviar Consulta" /></li>
                                                <li><input type="reset" value="Borrar" class="alt" /></li>
                                            </ul>
                                        </div>
                                    </div>
                                </form>
                        </div>
                    </div>
                </div>
            </section>

        <!-- Footer -->
            <footer id="footer">
                <div class="inner">
                    <div class="flex">
                        <div class="copyright">
                            &copy; Linea de Artes Marciales: <a href="https://lotosagradoweb.com" target="_blank">Loto Sagrado Web</a>.
                        </div>
                        <ul class="icons">
                            <li><a href="https://www.facebook.com/jafum.kenpo.7" target="_blank" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
                            <!--<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
                            <li><a href="#" class="icon fa-linkedin"><span class="label">linkedIn</span></a></li>
                            <li><a href="#" class="icon fa-pinterest-p"><span class="label">Pinterest</span></a></li>
                            <li><a href="#" class="icon fa-vimeo"><span class="label">Vimeo</span></a></li>-->
                        </ul>
                    </div>
                </div>
            </footer>

        <!-- Scripts -->
            <script src="{{ url('../resources/js/jquery.min.js') }}"></script>
            <script src="{{ url('../resources/js/skel.min.js') }}"></script>
            <script src="{{ url('../resources/js/util.js') }}"></script>
            <script src="{{ url('../resources/js/main.js') }}"></script>

    </body>
</html>