<!DOCTYPE HTML>
<!--
    Theory by TEMPLATED
    templated.co @templatedco
    Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html>
    <head>
        <title>Jafum Kenpo Studio</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="{{ url('../resources/css/main.css') }}" />
        <link rel="icon" type="image/png" href="{{url('../resources/images/jfumkenpologo.png')}}">
    </head>
    <body>

        <!-- Header -->
            <header id="header">
                <div class="inner">
                    <a href="index.html" class="logo"><img src="{{url('../resources/images/jfumkenpologo.png')}}" alt="logo jafumkenpo" /></a>
                    <nav id="nav">
                        <a href="{{ url('./') }}">Inicio</a>
                        <a href="{{ url('/contact') }}">Contactanos</a>
                        <!-- <a href="elements.html">Elementos</a> -->
                    </nav>
                    <a href="#navPanel" class="navPanelToggle"><span class="fa fa-bars"></span></a>
                </div>
            </header>

        <!-- Banner -->
            <section id="banner">
                <h1>Jafum Kenpo Studio</h1>
                <p>Escuela de Artes Marciales y Defensa Personal</p>
            </section>

        <!-- One -->
            <section id="one" class="wrapper">
                <div class="inner">
                    <div class="flex flex-3">
                        <article>
                            <header>
                                <h3>Kenpo Karate (Ed Parker)</h3>
                            </header>
                            <p>El American Kenpo Karate es un arte marcial y sistema de autodefensa moderno creado por el maestro Señorito Parker en Honolulu, Hawái, Estados Unidos.</p>
                            <footer>
                                <a href="#" class="button special">Leer Mas</a>
                            </footer>
                        </article>
                        <article>
                            <header>
                                <h3>Historia del Kenpo</h3>
                            </header>
                            <p>La historia moderna del Kenpo comienza en la década de 1940, cuando James Mitose (1916-1981) comienza a enseñar su ancestral arte marcial japonés Kosho-Ryu Kempo en Hawái.</p>
                            <footer>
                                <a href="#" class="button special">Leer Mas</a>
                            </footer>
                        </article>
                        <article>
                            <header>
                                <h3>Parcho IKKA o Universal</h3>
                            </header>
                            <p>El escudo conocido como "La Cresta" es el símbolo más representativo del Kenpo Karate Americano del Gran Maestro Ed Parker.</p>
                            <footer>
                                <a href="#" class="button special">Leer Mas</a>
                            </footer>
                        </article>
                    </div>
                </div>
            </section>

        <!-- Two -->
            <section id="two" class="wrapper style1 special">
                <div class="inner">
                    <header>
                        <h2>Instructores</h2>
                        <p>Con gran estima y calidad técnica</p>
                    </header>
                    <div class="flex flex-4">
                        <div class="box person">
                            <div class="image round">
                                <img src="{{url('../resources/images/masterjuan.jpg')}}" alt="Master Juan" />
                            </div>
                            <h3>Master <br> Juan Fuentes</h3>
                            <p>Cinturon negro 5° Dan <br> Kenpo Karate <br /> Drangon Kung Fu del Sur</p>
                        </div>
                        <div class="box person">
                            <div class="image round">
                                <img src="{{url('../resources/images/raguel.jpg')}}" alt="Sensei Raguel" />
                            </div>
                            <h3>Sensei <br> Ragüel Lares</h3>
                            <p>Cinnturon negro 2° Dan <br> Kenpo Karate</p>
                        </div>
                        <div class="box person">
                            <div class="image round">
                                <img src="{{url('../resources/images/martin.jpg')}}" alt="Sensei Martin" />
                            </div>
                            <h3>Sensei <br> Martin</h3>
                            <p>Cinturon negro <br> Kenpo Karate</p>
                        </div>
                        <!-- <div class="box person">
                            <div class="image round">
                                <img src="images/pic06.jpg" alt="Person 4" />
                            </div>
                            <h3>Dolore</h3>
                            <p>Praesent placer</p>
                        </div>-->
                    </div>
                </div>
            </section>

        <!-- Three -->
            <section id="three" class="wrapper special">
                <div class="inner">
                    <header class="align-center">
                        <h2>Eventos Recientes</h2>
                        <p>Noticias y eventos en los que participamos</p>
                    </header>
                    <div class="flex flex-2">
                        <article>
                            <div class="image fit">
                                <img src="{{url('../resources/images/federacioninternacional.jpg')}}" alt="Pic 01" />
                            </div>
                            <header>
                                <h3>Entrenamiento Federacion Shou Rou Jing</h3>
                            </header>
                            <p>Los sabados a las 9:00 AM hora de Caracas en la sede principal de la federacion en parque carabobo se hizo un entrenamiento unificado de las distintas artes marciales que la componen.</p>
                            <footer>
                                <a href="#" class="button special">Leer Mas</a>
                            </footer>
                        </article>
                        <article>
                            <div class="image fit">
                                <img src="{{url('../resources/images/entrenamientocial.jpg')}}" alt="Pic 02" />
                            </div>
                            <header>
                                <h3>Entrenamiento en el Colegio Cial</h3>
                            </header>
                            <p>Entrenamientos continuos los dias Martes, Jueves, Sabado y Domingo en semana radical en el colegio Cial.</p>
                            <footer>
                                <a href="#" class="button special">Leer Mas</a>
                            </footer>
                        </article>
                    </div>
                </div>
            </section>

        <!-- Footer -->
            <footer id="footer">
                <div class="inner">
                    <div class="flex">
                        <div class="copyright">
                            &copy; Linea de Artes Marciales: <a href="https://lotosagradoweb.com" target="_blank">Loto Sagrado Web</a>.
                        </div>
                        <ul class="icons">
                            <li><a href="https://www.facebook.com/jafum.kenpo.7" target="_blank" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
                            <!--<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
                            <li><a href="#" class="icon fa-linkedin"><span class="label">linkedIn</span></a></li>
                            <li><a href="#" class="icon fa-pinterest-p"><span class="label">Pinterest</span></a></li>
                            <li><a href="#" class="icon fa-vimeo"><span class="label">Vimeo</span></a></li>-->
                        </ul>
                    </div>
                </div>
            </footer>

        <!-- Scripts -->
            <script src="{{ url('../resources/js/jquery.min.js') }}"></script>
            <script src="{{ url('../resources/js/skel.min.js') }}"></script>
            <script src="{{ url('../resources/js/util.js') }}"></script>
            <script src="{{ url('../resources/js/main.js') }}"></script>

    </body>
</html>